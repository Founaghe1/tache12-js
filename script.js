// les nouveautes du ES6
//  utilisation de let et const
let x ;
x = 'moha';
console.log(x);

const y = 5;
console.log(y);

// l'utlisation de la function flechée (arg => value)

const myFunction = arg => arg * 5;
console.log(myFunction(2));

// ANCIENT
const me = {
    name: 'mohamed',
    presentFriend: function(friend){
        return "Connais tu "+ friend + " ?" ;   
    }
};

console.log(me.presentFriend('Sarifou'));

//NOUVEAU ES6
const moi = {
    name: 'mohamed',
    presentFriend: friend => "Connais tu "+ friend + " ?"     
};
console.log(moi.presentFriend('Sarifou Diallo'));

//SANS argument
const moi2 = {
    name: 'mohamed',
    presentMyself: () => "Hello cest moi!!!"    
};
console.log(moi2.presentMyself());

// PLUSIEURS arg
const presente = {
    name: 'mohamed',
    presentFriends: (friend1, friend2, friend3) => "Connais tu "+ friend1 +' '+friend2+' '+friend3+ ' ?'    
};
console.log(presente.presentFriends('Bah', 'Ndiaye', 'Gueye'));

//Avec une funtion
const funt = {
    nom : 'Diallo',
    presentFriend : friend => {
        const presentation = "connais tu " + friend + ' ?';
        console.log(presentation);
        return presentation;
    } 
};
funt.presentFriend("Mory");

// video 7